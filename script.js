const gameContainer = document.getElementById("game")

// const start = document.getElementById('start-button')

function startTheGame() {
    // show the boxes and score 
    document.getElementById('game').style.display = "block"
    document.getElementById('scoreBoard').style.display ="block"
    // hide the start button
    document.getElementById('start-button').style.display = "none"

}

const scoreElement = document.getElementById('score')



const COLORS = [
    "red",
    "blue",
    "green",
    "orange",
    "purple",
    "red",
    "blue",
    "green",
    "orange",
    "purple"
];

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
    let counter = array.length;

    // While there are elements in the array
    while (counter > 0) {
        // Pick a random index
        let index = Math.floor(Math.random() * counter);

        // Decrease counter by 1
        counter--;

        // And swap the last element with it
        let temp = array[counter];
        array[counter] = array[index];
        array[index] = temp;
    }

    return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
    for (let color of colorArray) {
        // create a new div
        const newDiv = document.createElement("div");

        // give it a class attribute for the value we are looping over
        newDiv.classList.add(color);

        // call a function handleCardClick when a div is clicked on
        newDiv.addEventListener("click", handleCardClick);

        // append the div to the element with an id of game
        gameContainer.append(newDiv);
    }
}

let noOfBox = COLORS.length
let matched = 0
let events = []
let score = 0
// TODO: Implement this function!
function handleCardClick(event) {

    let clickedEvent = event.target
    clickedEvent.style.backgroundColor = clickedEvent.classList.value
    
    // variable to count the number of perfect matches if its equal to noOfBox , then 
    // game is won 


    events.push(clickedEvent)
    //console.log(events)

    // bug clicking the same box again and again 
    // then it is bug prone, do a side check for that 
    if (events.length > 1) {
        if (events[0] === events[1]) {
            // just one pop is enough as it will regain the order.
            events.pop()
        }
    }

    if (events.length > 1) {
        if (events[0].classList.value === events[1].classList.value) {
            for (let box of events) {
                box.style.backgroundColor = box.classList.value
            }
            matched += 2
            score += 1
            scoreElement.innerText = score
            events.pop()
            events.pop()
        }

        else {
            // its going very fast set an timeout 

            setTimeout(() => {
            for (let box of events) {
                box.style.backgroundColor = "white";
            }
            events.pop()
            events.pop()
            },1000)
        }
    

    }
    
    console.log(noOfBox,matched)
// if matched equals noOfBoxes then stop the game

    if (noOfBox === matched) {
        setTimeout(() => {
            alert('Congrats , You have Good memory :)')
            window.location.reload();
        },500)
        
    }
}


// when the DOM loads
createDivsForColors(shuffledColors);
